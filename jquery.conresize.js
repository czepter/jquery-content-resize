(function($) {
	
	function getDimensions()
	{
		myWidth = 0;
		myHeight = 0;
		if(typeof(window.innerWidth)=='number')
		{
			//Non-IE
			myWidth = window.innerWidth;
			myHeight = window.innerHeight;
		}
		else
		{
			if(document.documentElement &&(document.documentElement.clientWidth || document.documentElement.clientHeight))
			{
				//IE 6+ in 'standards compliant mode'
				myWidth = document.documentElement.clientWidth;
				myHeight = document.documentElement.clientHeight;
			}
			else
			{
				if(document.body && (document.body.clientWidth || document.body.clientHeight))
				{
					//IE 4 compatible
					myWidth = document.body.clientWidth;
					myHeight = document.body.clientHeight;
				}
			}
		}
		
		return {"width":myWidth,"height":myHeight};
	}
	
	$.fn.conresize = function(options) {
		
		 var settings = $.extend( {
			 'offset'	: '50',
		    'min'		: '300'	  
		    }, options);

		return this.each(function(){
			var $this = $(this);
			
			if ($this == null)
				return;
				
			var dim = getDimensions();
						
			if (dim['height'] < settings.min)
				$this.css('min-height', settings.min - settings.offset);
			else
				$this.css('min-height', dim['height'] - settings.offset);		        
		});
	}
})(jQuery);